/*
 * FRI Tracker
 *
 * Copyright (c) 2020, Klemen Bratec
 */

#include <stdio.h>
#include <string.h>

#include <dk_buttons_and_leds.h>
#include <device.h>
#include <drivers/gps.h>
#include <drivers/watchdog.h>
#include <modem/lte_lc.h>
#include <modem/at_cmd.h>
#include <modem/at_notif.h>
#include <modem/modem_info.h>
#include <modem/modem_key_mgmt.h>
#include <modem/bsdlib.h>
#include <net/coap.h>
#include <net/socket.h>
#include <net/net_ip.h>
#include <net/tls_credentials.h>
#include <random/rand32.h>
#include <zephyr.h>

#include "gps_controller/gps_controller.h"

#include <logging/log.h>
LOG_MODULE_REGISTER(app);

/* Application work queue */
static struct k_work_q application_work_q;
static struct k_work send_data_work;

K_THREAD_STACK_DEFINE(application_stack_area, 2048);

/* Sockets, CoAP */
#define COAP_SERVER_HOSTNAME "thesis.bratec.si"
#define COAP_SERVER_PORT 5683

static char imei[16];
static int sock = -1;
static int64_t last_send_time_ms = 0;
static struct sockaddr_storage server;
static uint16_t next_token;
static uint8_t coap_buf[1280];

/* Certificate tags */
#define TLS_SEC_TAG_CA 1
#define TLS_SEC_TAG_CLIENT_CERT 2
#define TLS_SEC_TAG_CLIENT_KEY 3
 
/* Certificates and keys */
static const unsigned char ca_certificate[] = {
    #include "../cert/ca.pem"
};

static const unsigned char client_certificate[] = {
    #include "../cert/cert.pem"
};

static const unsigned char client_key[] = {
    #include "../cert/key.pem"
};

/* GPS */
static char gps_text_data[100];

/* Watchdog */
#define WDT_NODE DT_INST(0, nordic_nrf_watchdog)
#define WDT_DEV_NAME DT_LABEL(WDT_NODE)

static int wdt_channel_id;
static const struct device *wdt;
static struct wdt_timeout_cfg wdt_config;

/**@brief Recoverable BSD library error. */
void bsd_recoverable_error_handler(uint32_t err) {
    LOG_INF("bsdlib recoverable error: %u\n", (unsigned int)err);
}

/**@brief Resolve the configured hostname. */
static int server_resolve(void) {
    int err;

    struct addrinfo *result;
    struct addrinfo hints = {
        .ai_family = AF_INET,
        .ai_socktype = SOCK_DGRAM};

    char ipv4_addr[NET_IPV4_ADDR_LEN];

    err = getaddrinfo(COAP_SERVER_HOSTNAME, NULL, &hints, &result);

    if (err != 0) {
        LOG_INF("ERROR: getaddrinfo failed %d", err);
        return -EIO;
    }

    if (result == NULL) {
        LOG_INF("ERROR: Address not found");
        return -ENOENT;
    }

    struct sockaddr_in *server4 = ((struct sockaddr_in *)&server);

    server4->sin_addr.s_addr = ((struct sockaddr_in *)result->ai_addr)->sin_addr.s_addr;
    server4->sin_family = AF_INET;
    server4->sin_port = htons(COAP_SERVER_PORT);

    inet_ntop(AF_INET, &server4->sin_addr.s_addr, ipv4_addr, sizeof(ipv4_addr));
    LOG_INF("IPv4 Address found %s", log_strdup(ipv4_addr));

    freeaddrinfo(result);

    return 0;
}

/**@brief "Connect" using DTLS to the remote CoAP server */
static int client_connect(void) {
    int err;

    sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_DTLS_1_2);

    if (sock < 0) {
        LOG_ERR("Failed to create UDP socket: %d.", errno);
        return -errno;
    }

    sec_tag_t sec_tag_list[] = {
        TLS_SEC_TAG_CA,
        TLS_SEC_TAG_CLIENT_CERT,
        TLS_SEC_TAG_CLIENT_KEY,
    };

    err = setsockopt(sock, SOL_TLS, TLS_SEC_TAG_LIST, sec_tag_list, sizeof(sec_tag_list));

	if (err < 0) {
        LOG_ERR("Failed to set secure option (%d)", -errno);
		return -errno;
	}

    err = setsockopt(sock, SOL_TLS, TLS_HOSTNAME, COAP_SERVER_HOSTNAME, sizeof(COAP_SERVER_HOSTNAME));

	if (err < 0) {
        LOG_ERR("Failed to set DTLS hostname option (%d)", -errno);
		return -errno;
	}

    int pv = TLS_PEER_VERIFY_REQUIRED;
    err = setsockopt(sock, SOL_TLS, TLS_PEER_VERIFY, &pv, sizeof(TLS_PEER_VERIFY_REQUIRED));

	if (err < 0) {
        LOG_ERR("Failed to set peer verify option (%d)", -errno);
		return -errno;
	}

    err = connect(sock, (struct sockaddr *)&server, sizeof(struct sockaddr_in));

    if (err < 0) {
        LOG_ERR("Connect failed: %d", errno);
        return -errno;
    }

    /* Randomize token. */
    next_token = sys_rand32_get();

    return 0;
}

/**@brief Send CoAP POST request. */
static int client_post_send(char *resource, char *payload, int payload_size) {
    int err;
    struct coap_packet request;

    next_token++;

    err = coap_packet_init(&request, coap_buf, sizeof(coap_buf), 1,
        COAP_TYPE_NON_CON, sizeof(next_token),
        (uint8_t *)&next_token, COAP_METHOD_POST, coap_next_id());

    if (err < 0) {
        LOG_ERR("Failed to create CoAP request, %d", err);
        return err;
    }

    // Split path on "/" and append every part of the path as a separate option
    char delim[] = "/";
    char *token = strtok(resource, delim);

    while (token != NULL) {
        err = coap_packet_append_option(&request, COAP_OPTION_URI_PATH,
            (uint8_t *)token, strlen(token));

        if (err < 0) {
            LOG_ERR("Failed to encode CoAP option, %d", err);
            return err;
        }

        token = strtok(NULL, delim);
    }

    err = coap_packet_append_payload_marker(&request);

    if (err < 0) {
        LOG_ERR("Failed to add CoAP payload marker, %d", err);
        return err;
    }

    err = coap_packet_append_payload(&request, (uint8_t *)payload, payload_size);

    if (err < 0) {
        LOG_ERR("Failed to append CoAP payload, %d", err);
        return err;
    }

    err = send(sock, request.data, request.offset, 0);

    if (err < 0) {
        LOG_ERR("Failed to send CoAP request, %d", err);
        return -errno;
    }

    LOG_INF("CoAP request sent: token 0x%04x", next_token);

    return 0;
}

/**@brief Configure modem, read IMEI and connect to the mobile network. */
static int modem_configure_connect(void) {
    int err;

    err = modem_info_init();

    if (err != 0) {
        LOG_ERR("Could not read modem information, %d", err);
        return -errno;
    }

    err = modem_info_string_get(MODEM_INFO_IMEI, (char *)&imei, 16);

    if (err < 0) {
        LOG_ERR("Could not read modem IMEI, %d", err);
        return -errno;
    }

    LOG_INF("Modem IMEI: %s", log_strdup(imei));

    LOG_INF("Link connecting ...");

    err = lte_lc_init_and_connect();

    if (err < 0) {
        LOG_ERR("Link could not be established, %d", err);
        return -errno;
    }

    LOG_INF("Link connected");

    return 0;
}

/**@brief Enable GPS location fetching. */
static void gps_enable() {
    if (gps_control_is_enabled()) {
        return;
    }

    LOG_INF("Starting GPS");
    gps_control_start(K_MSEC(3000));
}

/**@brief Handle GPS events. */
static void gps_handler(const struct device *dev, struct gps_event *evt) {
    switch (evt->type) {
    case GPS_EVT_SEARCH_STARTED:
        LOG_INF("GPS_EVT_SEARCH_STARTED");
        gps_control_set_active(true);
        break;
    case GPS_EVT_SEARCH_STOPPED:
        LOG_INF("GPS_EVT_SEARCH_STOPPED");
        gps_control_set_active(false);
        break;
    case GPS_EVT_SEARCH_TIMEOUT:
        LOG_INF("GPS_EVT_SEARCH_TIMEOUT");
        gps_control_set_active(false);
        break;
    case GPS_EVT_PVT:
        break;
    case GPS_EVT_PVT_FIX:
        LOG_INF("Position fix with PVT data");

        sprintf(gps_text_data, "%f,%f,%f", evt->pvt.latitude, evt->pvt.longitude, evt->pvt.altitude);

        LOG_INF("Got location: %s", log_strdup((const char *)gps_text_data));

        k_work_submit_to_queue(&application_work_q, &send_data_work);

        break;
    case GPS_EVT_NMEA:
        break;
    case GPS_EVT_NMEA_FIX:
        LOG_INF("Position fix with NMEA data");
        break;
    case GPS_EVT_OPERATION_BLOCKED:
        LOG_INF("GPS_EVT_OPERATION_BLOCKED");
        break;
    case GPS_EVT_OPERATION_UNBLOCKED:
        LOG_INF("GPS_EVT_OPERATION_UNBLOCKED");
        break;
    case GPS_EVT_AGPS_DATA_NEEDED:
        LOG_INF("GPS_EVT_AGPS_DATA_NEEDED");
        break;
    case GPS_EVT_ERROR:
        LOG_INF("GPS_EVT_ERROR\n");
        break;
    default:
        break;
    }
}

/**@brief Send out GPS data via CoAP. */
static void send_data_work_fn(struct k_work *work) {
    char resource[33];

    // Close the socket if a lot of time has passed since the last send
    if (k_uptime_get() - last_send_time_ms > 27000 && sock > -1) {
        LOG_INF("Dropping connection, session too old");
        close(sock);
        sock = -1;
    }

    // Initialize connection if the socket hasn't been opened yet
    if (sock < 0) {
        LOG_INF("Establishing client connection...");
        client_connect();
    }

    sprintf(resource, "devices/%s/location", imei);

    LOG_INF("Posting to resource: %s", log_strdup(resource));

    if (client_post_send(resource, gps_text_data, strlen(gps_text_data)) != 0) {
        LOG_INF("Failed to send CoAP POST request");
    }

    last_send_time_ms = k_uptime_get();

    gps_control_start(K_MSEC(1000));
}

/**@brief Provision (D)TLS certificates to the modem. */
static int cert_provision(void)
{
    int err;

    err = modem_key_mgmt_write(TLS_SEC_TAG_CA,
        MODEM_KEY_MGMT_CRED_TYPE_CA_CHAIN,
        ca_certificate,
        sizeof(ca_certificate) - 1);

    if (err < 0) {
        LOG_ERR("Failed to register CA certificate: %d", err);
        return -errno; 
    }

    err = modem_key_mgmt_write(TLS_SEC_TAG_CLIENT_CERT,
        MODEM_KEY_MGMT_CRED_TYPE_PUBLIC_CERT,
        client_certificate,
        sizeof(client_certificate) - 1);

    if (err < 0) {
        LOG_ERR("Failed to register client certificate: %d", err);
        return -errno; 
    }

    err = modem_key_mgmt_write(TLS_SEC_TAG_CLIENT_KEY,
        MODEM_KEY_MGMT_CRED_TYPE_PRIVATE_CERT,
        client_key,
        sizeof(client_key) - 1);

    if (err < 0) {
        LOG_ERR("Failed to register client key: %d", err);
        return -errno; 
    }

    return 0;
}

/**@brief Initialize the watchdog. */
static int wdt_init(void)
{
    wdt = device_get_binding(WDT_DEV_NAME);

    if (!wdt) {
        LOG_ERR("Cannot get WDT device");
        return -1;
    }

    wdt_config.flags = WDT_FLAG_RESET_SOC;
    wdt_config.window.min = 0U;
    wdt_config.window.max = 3000U;

    wdt_channel_id = wdt_install_timeout(wdt, &wdt_config);

    if (wdt_channel_id < 0) {
        LOG_ERR("Watchdog install error");
        return -1;
    }

    int err = wdt_setup(wdt, 0);

    if (err < 0) {
        LOG_ERR("Watchdog setup error");
        return err;
    }

    return 0;
}

void main(void) {
    int err;

    LOG_INF("FRI Tracker started");

    // Prepare and initialize work queue
    k_work_q_start(&application_work_q, application_stack_area,
        K_THREAD_STACK_SIZEOF(application_stack_area),
        -1);

    k_work_init(&send_data_work, send_data_work_fn);

    // Initialize LEDs
    err = dk_leds_init();

    if (err != 0) {
        LOG_ERR("Failed to initialize LEDs");
        return;
    }

    // Turn off all LEDs
    dk_set_leds_state(DK_NO_LEDS_MSK, DK_ALL_LEDS_MSK);

    // Provision CA and client certificates
    err = cert_provision();

	if (err != 0) {
        LOG_ERR("Failed to provision certificates");
		return;
	}
    
    // Configure modem, connect to the network
    err = modem_configure_connect();

	if (err != 0) {
        LOG_ERR("Failed to configure modem and/or connect to the network");
		return;
	}

    // Turn on LED 1
    dk_set_led_on(DK_LED1);

    // Resolve CoAP server hostname
    err = server_resolve();

    if (err != 0) {
        LOG_ERR("Failed to resolve server name");
        return;
    }

    // Register GPS handler
    err = gps_control_init(&application_work_q, gps_handler);

    if (err != 0) {
        LOG_ERR("GPS could not be initialized");
        return;
    }

    // Initialize the watchdog
    err = wdt_init();

    if (err != 0) {
        LOG_ERR("Failed to initialize the watchdog");
        return;
    }

    // Enable GPS
    gps_enable();

    // Feed the watchdog
    while (1) {
        wdt_feed(wdt, wdt_channel_id);
        k_sleep(K_MSEC(1000));
    }
}